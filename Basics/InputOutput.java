// Java program for input and output demo.,

import java.util.*;         // Importing the Scanner classes.
public class InputOutput{

    public static void main(String [] args){

        // Output statement in java
        System.out.print("Hello world");        // Prints in one line.,
        System.out.println("Welcome to Java!"); // Prints in new line.,

        
         int dob;
         String name;

         // Input Statement in java,
         Scanner sc = new Scanner(System.in);  // Creating an Scanner object.,
         System.out.println("Enter yor year of birth:");
         dob = sc.nextInt();                     // Reading input from the keyboard using Scanner object(sc).,
          
          System.out.println("Enter your name:");
          name = sc.next();

            int age = 2023 -dob;

            System.out.println("Hello "+name+" You are "+age+ " years old");
            
    }
}