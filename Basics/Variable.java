// Variable - name given for memory location.,


class Variable{

    static int i = 0;                           // Static variable,
    int a = 10;                                 // Instance variable.,
    public static void main(String[] args){
        int b = 20;                             // Local Variable.,

        System.out.println("Static Variable " +i);                // 0, because static variables can be accessed directly,
        System.out.println("Local Variable " +b);                  // 10, local variables can be directly accessed.,
       // System.out.println(a);                  // Not Possible ,instance variable can't be accessed directly.,
    
        // Using the object to access the local variable

        Variable v = new Variable();
        System.out.println("Instance Variable " +v.a);
    }
}