// Static block, Method, Variable.,


    class D{
        static int a = 2;

        static void aa(){
            System.out.println("Staic Method of another class");
        }

        static {                                                                   // Static Block
                System.out.println("Static Block");
        }
    }
    class StaticBlock{

        static int t = 1;                                                       // Static variable.,

            static void display(){                                              // Static method.,
                System.out.println("Static Method can be accessed diractly");
            }

        public static void main(String [] args){

            System.out.println(t);
            display();

            System.out.println(D.a);                                               // Static variable accessing using class name.,
            D.aa();
        }
    }