// Constructor 

class Constructor{

    String name;
    int id;

     Constructor() {                                     
        name = "Ana";
        id = 1;
    }


    Constructor(String nam, int i) {                                     
        name = nam;
        id = i;
    }
    public static void main(String [] args){

        Constructor st = new Constructor();             // Constructor() is the default constructor created 
         Constructor st1 = new Constructor("Ben",2);    // Parameterized Constructor
        System.out.println(st.name);
        System.out.println(st.id);

        System.out.println(st1.name);
        System.out.println(st1.id);

    }
}