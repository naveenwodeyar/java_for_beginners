// String Methods in String Class

    class StringMethods{

        public static void main(String [] args){
            
            String str = " Hello";
            System.out.println(str);

            String str1 = str.concat("Good Morning");               // Concatenates & prints Hello Good Morning.,
            System.out.println(str1);

            System.out.println(str1.length());                      // length() prints 17 , the length of the given string.,
            
            System.out.println(str1.indexOf("G"));                  // Prints 5 which is the index of G in the given string.,

            System.out.println(str1.charAt(5));                     // Prints H, which is present at the index 5.,

            // toLowerCase()
            System.out.println(str.toLowerCase());

            // toUpperCase();
            System.out.println(str.toUpperCase());

            // trim()  -removes white spaces
            System.out.println(str1.trim());

            // compareTo()
            System.out.println(str1.compareTo("Morning"));



        }
    }