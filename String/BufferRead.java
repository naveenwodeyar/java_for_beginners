// Reading input using the BufferReader class, which is present in java.io package.,

    import java.io.*;
    class BufferRead{

        public static void main(String[] args) throws Exception {

            // InputStreamReader ir = new InputStreamReader(System.in);        // Reading the data through InputStreamReader object, bytes converted into characters.,
            // BufferedReader br = new BufferedReader(ir);                         //  


            // System.out.println("Enter your Age:");
            // int a = Integer.parseInt(br.readLine());                        // Reading integer value.,
            
            // System.out.println("Enter yor Salary:");
            // float f = Float.parseFloat(br.readLine());                      // Reading float value.,
            
            // System.out.println("Enter yor name:");
            // String str = br.readLine();                                     // Reading string from keyboard.,
        
            // Reading from file

            FileReader f = new FileReader();                    // Reading from a file using FileReader object.,
            BufferedReader br = new BufferedReader(f);
            String str = br.readLine();
            System.out.println(str);
        }  
    }