// StringBuffer class 

    class StringBuffr{

        public static void main (String [] args){

            StringBuffer sb = new StringBuffer("Good Morning");                 // Creating an StringBuffer object.,
            
            System.out.println(sb);

            // append()
            System.out.println(sb.append("  Welcome ")); 

            // length()
            System.out.println(sb.length()); 

            //insert()
            System.out.println(sb.insert(22, "Mr"));

            // delete()
            System.out.println(sb.delete(22,24));

            // reverse()
            System.out.println(sb.reverse());
            
            // reverse()
            System.out.println(sb.reverse());

            // indexOf()
            System.out.println(sb.indexOf("d"));
            
            // charAt()
            System.out.println(sb.charAt(15));

            // substring()
            System.out.println(sb.substring(5));

            // subSequence()
            System.out.println(sb.subSequence(0,5));

            System.out.println(sb);
            
        }   
    }