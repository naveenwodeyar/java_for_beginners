// Switch case to display the name of the day on getting the number from the user using Scanner object.

    import java.util.*;
    class Swtch{

        public static void main(String args[]){
            Scanner sc = new Scanner(System.in);
                System.out.println("Enter the day number:");
                 int dayNo = sc.nextInt();

                switch (dayNo){
                    case 1:
                            System.out.println("Today is Monday");
                                break;
                    case 2:
                            System.out.println("Today is Tuesday");
                                break;
                    case 3:
                            System.out.println("Today is Wednesday");
                                break;
                    case 4:
                            System.out.println("Today is Thursday");
                                break;
                    case 5:
                            System.out.println("Today is Friday");
                                break;
                    case 6:
                            System.out.println("Today is Saturday");
                                break;
                    case 7:
                            System.out.println("Today is Sunday");
                                break;
                    default :
                            System.out.println("Enter the valid number");
                                break;
                }
        }
    }