// Applets in the java.

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.*;


    public class Applt extends Applet{

        public void init(){
            setBackground(Color.black);
            setForeground(Color.yellow);
        }

        public void paint(Graphics g){
            g.drawString("Welcome to the Applet", 150,150);
        }
     
    }