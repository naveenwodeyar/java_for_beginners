// Method Overloading 

    class Overload1{

        void add(int a, int b) {
            System.out.println("Addition of two Numbers " + "= " + (a+b));
        
        }

        void add(int a, int b, int c) {
            System.out.println("Addition of three Numbers " +"= " +(a+b+c));
       
        }

        public static void main(String [] args){
            Overload1 a = new Overload1();
                
                a.add(1,1);
                a.add(1,1,1);
        }
    }