// Operator Overloading (+ used as both addition & concatenation)., 

    class Overload2{

        int a = 1, b = 1;
        public static void main(String [] args){
            Overload2 ob = new Overload2();                         // Creating an object, compile time polymorphism.,
                System.out.println("a+b=" + (ob.a + ob.b) );
        }
    }