// Constructor Overloading 

    class Overload{

        Overload() {
            System.out.println("Constructor without Arguments");
        }

        Overload(int a) {
            System.out.println("Constructor with Arguments");

        }

        public static void main(String [] args){
            Overload a = new Overload();
            Overload aa = new Overload(10);
        }
    }