// this keyword in java.,

    class This{

        int a = 1, b = 1;       // Instance variables.,

         void add(){
            int a = 2;
            System.out.println(a);          //2 because it is local variable
            System.out.println(this.a);     // 1 because this is used to 
             
        }
        public static void main(String args[]){
         
            This ob = new This();
            ob.add();
        }
    }