// Method Overriding.


    class Parent {
         
         int a=1, b=1;
        int add(){
            System.out.println("Parent Class Method");
            System.out.println("Adiiton value = " + (a+b));
            return a+b;
        }
    }

    class Child extends Parent{
        int a = 1, b = 1, c = 1;
            int add(){
            System.out.println("Child Class Method");
            System.out.println("Adiiton value = " + (a+b+c));
            
            //
            super.add();  
            return a+b;
            
        }
    }
    class OverRide{

        public static void main(String args[]){

            Child ob = new Child();
            ob.add();
        }
    }