// Abstract class, methods

abstract class A{
    abstract void disp();
    void tell(){
        System.out.println("Normal Method");
    }
}
// Concrete class
class B extends A{
    void disp(){
        System.out.println("Concrete Class");
    }
}
    class Abstract{
        public static void main(String [] args){
            B ob = new B();
            ob.disp();
            ob.tell();
         }
    }