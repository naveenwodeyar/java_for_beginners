// Inheritance in java(extends).,


    class Parent {
        int houseNumber = 1;
        String place = "Bella-Bella";
        
        void tell(){
        System.out.println("Parent Class");
        }
    }
        class Child extends Parent{
            int age = 23;
            String plac = "Canbera";
            
            void tells(){
                System.out.println("Child Class");
            }
        }

    class Single{
        public static void main (String [] args){

            // Constructor of the child class.,
            Child ch = new Child();            

            ch.tell();
            System.out.println(ch.houseNumber);         
            System.out.println(ch.place);         

            ch.tells();
            System.out.println(ch.age);         
            System.out.println(ch.plac);         

        }
    }
