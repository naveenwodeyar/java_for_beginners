// Heirarchical Inheritance 

    class GrandFather{
        String name = "Subramani Wodeyar";
        void tel(){
            System.out.println("GrandFather");
            System.out.println("My Name is "+ name);
        }
    }

    class Father extends GrandFather{
        String name = "Krishnappa Wodeyar";
        void tell(){
            System.out.println("Father");
            System.out.println("My Name is "+ name);
        }
    }

    class Son extends GrandFather{
        String name = "Naveen Wodeyar";
        void tells(){
            System.out.println("Son");
            System.out.println("My Name is "+ name);
        }
    }
    class Heirarchical{
        public static void main (String [] args){
            // Creating the constructor.,
            Son ob = new Son();          
            Father obb = new Father();
            
            ob.tel();
            obb.tell();
            ob.tells();                   
        }        
    }