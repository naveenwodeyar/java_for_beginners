// Super keyword

    class A{
        A(){
            System.out.println("Parent Constructor");
        }
        int id = 1;
        String clg = "Oxford";   

        void disp(){
            System.out.println("*********");
        }
    }

    class B extends A{

        B(){
            System.out.println("Child Constructor");
        }

        int id = 2;
        String clg = "Mt Carmel";

        void disp() {
        System.out.println("College Id: "+ super.id);
        System.out.println("College Name: "+ super.clg);
        
        System.out.println("Child Class variables");
        System.out.println("College Id: "+ id);
        System.out.println("College Name: "+ clg);
        // recursion, because we are not specifying the super.,
        //disp();
        }
    }

    class Super {

        public static void main(String [] args){
            B ob = new B();
            System.out.println("Main Method");

            ob.disp();
        }
    }